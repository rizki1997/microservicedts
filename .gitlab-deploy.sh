set -f  
echo "Deploy project on server ${DEPLOY_SERVER}"    
ssh ubuntu@$DEPLOY_SERVER "cd /home/ubuntu/microservicedts && git pull origin master"
