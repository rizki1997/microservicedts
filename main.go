package main
import (
        "fmt"
        "net/http"
)
func hello(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintf(w, "hello Kiswono Prayogo\n")
}
func main() {
        http.HandleFunc("/", hello)
        fmt.Println(`listen at :8090`)
        fmt.Println(http.ListenAndServe(":8090", nil))
}
